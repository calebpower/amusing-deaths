package com.calebpower.mc.amusingdeaths;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.json.JSONObject;

import com.calebpower.mc.amusingdeaths.deathmethods.*;

public class Core extends JavaPlugin {
  
  /**
   * Handles all commands and facilitates instantiation of death methods
   * 
   * @author LordInateur (Caleb L. Power)
   */
  public static final Death DEATH_TYPES[] = { //this array is a list of all available death methods
    new AnvilsDeath(),     //death by anvil
    new FireDeath(),       //death by fire
    new FreefallDeath(),   //death by freefall
    new InstantDeath(),    //death instantaneously
    new LightningDeath(),  //death by electrocution
    new MobsDeath()        //death by mobs
  };
	
  /**
   * Give credit when the plugin is enabled.
   */
	@Override public void onEnable() {
	  Messenger.log("Created by LordInateur.");
	}
	
	/**
	 * Notify console that the plugin is being disabled.
	 */
	@Override public void onDisable() {
	  Messenger.log("Disabling plugin...");
	}
	
	/**
	 * Handle all commands.
	 */
	@Override public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
	  
	  if(args.length == 1 && args[0].equalsIgnoreCase("list")) { //list all ways in which to kill someone
	    
	    Messenger.sendMessage(sender, "*---------------------------------------*");
      Messenger.sendMessage(sender, "Suggestions for killing targets:");
	    for(int i = 0; i < DEATH_TYPES.length; i++) {
        Messenger.sendMessage(sender, "- *" + DEATH_TYPES[i].getName() + "* -> " + DEATH_TYPES[i].getDescription());
      }
	    Messenger.sendMessage(sender, "*---------------------------------------*");
	    
	  } else if (args.length == 2) { //invoke this when a target is being killed
	    
	    Player player = validatePlayer(args[0]); //get a valid player (and get null if they are not online)
	    
	    if(player == null) { //do this if the player is not online (or doesn't exist)
	      Messenger.sendMessage(sender, "*---------------------------------------*");
	      Messenger.sendMessage(sender, "That player is not online.", Messenger.MessageType.ERROR);
	      Messenger.sendMessage(sender, "*---------------------------------------*");
	      return true;
	    }
	    
	    Death death = null;
	    
	    for(int i = 0; i < DEATH_TYPES.length; i++) { //match the death method to the command sender's request
        if(DEATH_TYPES[i].getName().equalsIgnoreCase(args[1])) {
          death = DEATH_TYPES[i];
          break;
        }
	    }
	    
	    Messenger.sendMessage(sender, "*---------------------------------------*");
	    if(death == null) { //double-check to ensure that a death method was properly specified
	      Messenger.sendMessage(sender, "That particular method of death could not be carried out.", Messenger.MessageType.ERROR);
	    } else {
	      Messenger.sendMessage(sender, "You have just executed *" + player.getName() + "*.", Messenger.MessageType.SUCCESS);
	      death.execute(player); //kill the player here
	    }
	    Messenger.sendMessage(sender, "*---------------------------------------*");
	    
	  } else { //display usage if the command sender had some syntax error
	    
	    Messenger.sendMessage(sender, "*---------------------------------------*");
	    Messenger.sendMessage(sender, "Usage:");
	    Messenger.sendMessage(sender, "- */murder list* -> list all methods");
	    Messenger.sendMessage(sender, "- */murder <player> <method>* -> kill a player");
	    Messenger.sendMessage(sender, "*---------------------------------------*");
	    
	  }
	  
	  return true; //don't display the default Bukkit usage message
	  
	}
	
	/**
	 * Given some username, retrieve a player if they're online
	 * 
	 * @param playerName the name of the player in question
	 * @return Player if the player is online or <code>null</code> if the player
	 *         was not found online.
	 */
	private Player validatePlayer(String playerName) {
	  
	  try { //try to log into Mojang servers to retrieve UUID of specified player
	    
      URL mojangPlayerAPI = new URL("https://api.mojang.com/users/profiles/minecraft/" + playerName);
      BufferedReader in = new BufferedReader(new InputStreamReader(mojangPlayerAPI.openStream()));
      String response = new String();
      for(String line = new String(); (line = in.readLine()) != null;) response += line;
      in.close();
      
      JSONObject jsonObject = new JSONObject(response); //parse the JSON response
      for(Player player : Bukkit.getOnlinePlayers()) { //match the player to the user-requested target
        if(player.getUniqueId().toString().replace("-", "").equalsIgnoreCase(jsonObject.getString("id"))) return player;
      }
      
    } catch (Exception e) { }
	  
	  return null;
	  
	}

}
