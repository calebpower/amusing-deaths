package com.calebpower.mc.amusingdeaths.deathmethods;

import org.bukkit.GameMode;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

/**
 * Death by mobs.
 * 
 * @author LordInateur (Caleb L. Power)
 */
public class MobsDeath extends Death {

  /**
   * Default constructor to set the name and description of the death method in question.
   */
  public MobsDeath() {
    name = "mobs";
    description = "send mobs after the target";
  }
  
  /**
   * Define the specific method of execution for the target in question.
   */
  @Override public void execute(Player player) {
    player.setGameMode(GameMode.SURVIVAL);
    for(int i = 0; i < 50; i++) {
      switch(i % 5) {
      case 0:
        player.getWorld().spawnEntity(player.getLocation(), EntityType.ZOMBIE);
        break;
      case 1:
        player.getWorld().spawnEntity(player.getLocation(), EntityType.SPIDER);
        break;
      case 2:
        player.getWorld().spawnEntity(player.getLocation(), EntityType.CREEPER);
        break;
      case 3:
        player.getWorld().spawnEntity(player.getLocation(), EntityType.WITCH);
        break;
      case 4:
        player.getWorld().spawnEntity(player.getLocation(), EntityType.SKELETON);
      }
    }
  }

}
