package com.calebpower.mc.amusingdeaths.deathmethods;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 * Death by anvil.
 * 
 * @author LordInateur (Caleb L. Power)
 */
public class AnvilsDeath extends Death {

  /**
   * Default constructor to set the name and description of the death method in question.
   */
  public AnvilsDeath() {
    name = "anvils";
    description = "drop a ton of anvils on the target";
  }
  
  /**
   * Define the specific method of execution for the target in question.
   */
  @Override public void execute(Player player) {
    player.setGameMode(GameMode.SURVIVAL);
    Location playerLocation = player.getLocation();
    for(int y = 20; y < 25; y++) {
      for(int x = -1; x <= 1; x++) {
        for(int z = -1; z <= 1; z++) {
          Location anvilLocation = new Location(playerLocation.getWorld(), playerLocation.getX() + x, playerLocation.getY() + y, playerLocation.getZ() + z);
          anvilLocation.getBlock().setType(Material.ANVIL);
        }
      }
    }
  }

}
