package com.calebpower.mc.amusingdeaths.deathmethods;

import org.bukkit.GameMode;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

/**
 * Death by lightning.
 * 
 * @author LordInateur (Caleb L. Power)
 */
public class LightningDeath extends Death {

  /**
   * Default constructor to set the name and description of the death method in question.
   */
  public LightningDeath() {
    name = "lightning";
    description = "electrocute the target";
  }
  
  /**
   * Define the specific method of execution for the target in question.
   */
  @Override public void execute(Player player) {
    player.setGameMode(GameMode.SURVIVAL);
    for(int i = 0; i < 10; i++) {
      player.getWorld().spawnEntity(player.getLocation(), EntityType.LIGHTNING);
    }
  }

}
