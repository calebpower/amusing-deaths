package com.calebpower.mc.amusingdeaths.deathmethods;

import org.bukkit.entity.Player;

/**
 * Abstract class to polymorphically handle different death methods
 * 
 * @author LordInateur (Caleb L. Power)
 */
public abstract class Death {
  
  String description = "No description.";
  String name = "N/A";

  /**
   * Kill the target player.
   * 
   * @param player the target to be executed
   */
  public abstract void execute(Player player);
  
  /**
   * Retrieve the description of the death method.
   * 
   * @return String denoting the death method's description
   */
  public String getDescription() {
    return description;
  }

  /**
   * Retrieve the name of the death method.
   * 
   * @return String denoting the death method's name
   */
  public String getName() {
    return name;
  }
  
}
