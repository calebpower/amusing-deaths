package com.calebpower.mc.amusingdeaths.deathmethods;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;

/**
 * Death by fire.
 * 
 * @author LordInateur (Caleb L. Power)
 */
public class FireDeath extends Death {
  
  /**
   * Default constructor to set the name and description of the death method in question.
   */
  public FireDeath() {
    name = "fire";
    description = "set the target on fire";
  }
  
  /**
   * Define the specific method of execution for the target in question.
   */
  @Override public void execute(Player player) {
    player.setGameMode(GameMode.SURVIVAL);
    player.setFireTicks(5000);
  }

}
