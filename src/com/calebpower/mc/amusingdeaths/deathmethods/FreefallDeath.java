package com.calebpower.mc.amusingdeaths.deathmethods;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * Death by freefall.
 * 
 * @author LordInateur (Caleb L. Power)
 */
public class FreefallDeath extends Death {
  
  /**
   * Default constructor to set the name and description of the death method in question.
   */
  public FreefallDeath () {
    name = "freefall";
    description = "drop the target to their death";
  }
  
  /**
   * Define the specific method of execution for the target in question.
   */
  @Override public void execute(Player player) {
    Location oldLocation = player.getLocation();
    Location newLocation = new Location(oldLocation.getWorld(), oldLocation.getX(), oldLocation.getY() + 500, oldLocation.getZ());
    player.setGameMode(GameMode.SURVIVAL);
    player.teleport(newLocation);
  }

}
