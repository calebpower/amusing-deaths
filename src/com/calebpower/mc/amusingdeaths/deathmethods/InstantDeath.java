package com.calebpower.mc.amusingdeaths.deathmethods;

import org.bukkit.entity.Player;

/**
 * Instantaneous death.
 * 
 * @author LordInateur (Caleb L. Power)
 */
public class InstantDeath extends Death {
  
  /**
   * Default constructor to set the name and description of the death method in question.
   */
  public InstantDeath() {
    name = "instant";
    description = "instantly kill the target";
  }
  
  /**
   * Define the specific method of execution for the target in question.
   */
  @Override public void execute(Player player) {
    player.setHealth(0.0);
  }

}
